<?php

namespace App\Entities\Posts;

use App\Entities\BaseRepository;

class PostsRepository extends BaseRepository
{

	public function __construct(Post $model)
	{
		parent::__construct($model);
	}

}
