@extends('oxygen::layouts.master-dashboard')

@section('breadcrumbs')
    {{ lotus()->breadcrumbs([
        ['Dashboard', route('dashboard')],
        [$pageTitle, null, true]
    ]) }}
@stop

@section('pageMainActions')
    @include('oxygen::dashboard.partials.searchField')

    {{--<a href="{{ entity_resource_path() . '/create' }}" class="btn btn-success"><em class="fas fa-plus-circle"></em> Add New</a>--}}
@stop

@section('content')
    @include('oxygen::dashboard.partials.table-allItems', [
        'tableHeader' => [
            'ID', 'Title', 'Body', 'Actions'
        ]
    ])

    @foreach ($allItems as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>
                {{ $item->title }}
            </td>
            
            <td>
               {{ $item->body }}
            </td>
            <td>
                
                    <form action="{{ entity_resource_path() . '/' . $item->id }}"
                          method="POST" class="form form-inline js-confirm-delete">
                        {{ method_field('delete') }}
                        @csrf
                        <button class="btn btn-danger js-tooltip"
                                title="PERMANENTLY DELETE. THIS CANNOT BE REVERSED!"><em class="fa fa-times"></em> Delete</button>
                    </form>
               
            </td>
        </tr>
    @endforeach
@stop
